import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:math';

void main() {
  runApp(GetMaterialApp(home:MyApp()));
}

class LocationController extends GetxController {
  var x = 0.0.obs;
  var y = 0.0.obs;
  changeX(double xx){
    x.value = xx;
    y.value = f(xx);
  }

  double f(x) {
    double y = sin(x);
    return y;
  }
}


class Ball extends StatelessWidget {
  // Ball({Key? key,double? radius = 15,Color? color=Colors.blue, this.radius, this.color,});
  Ball({Key? key, this.radius=15, this.color=Colors.blue,}) :super(key: key);
  final double radius;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: radius*2,
      height:radius*2,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
      ),
    );
  }
}




class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final LocationController c = Get.put(LocationController());
    return Scaffold(
      appBar: AppBar(title: Text("Alignment test")),
      body: Center(
        child:Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Obx(()=>buildSlider(c)),
            Obx(()=>buildItem(c)),
          ],
        ),
      ),
    );

  }

  Widget buildSlider(LocationController c) {
    return Slider(
      max: 180,
      min: -180,
      divisions: 360,
      label: "${c.x.value.toStringAsFixed(2)}π",
      value: c.x.value*180,
      onChanged:(v){
        c.changeX(v/180);
      },
    );
  }
  
  
  Widget buildItem(LocationController c) {
    return Container(
      width: 500,
      height:200,
      color: Colors.black.withAlpha(10),
      child: Align(
        child: Ball(color:Colors.orangeAccent,),
        alignment:Alignment(c.x.value,c.y.value),
      ),
    );
  }
}

